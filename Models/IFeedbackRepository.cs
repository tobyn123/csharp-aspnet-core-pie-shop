﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PieShoppluralsight.Models
{
    interface IFeedbackRepository
    {
        void AddFeedback(Feedback feedback);

    }
}
